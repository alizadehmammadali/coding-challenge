package com.geekforless.codingchallange.client;

import com.geekforless.codingchallange.model.dto.binance.BinanceSymbolPayload;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient(name = "binance-client", url = "${application.services.binance.url}")
public interface BinanceClient {

    @GetMapping("/exchangeInfo")
    BinanceSymbolPayload getSymbols();
}
