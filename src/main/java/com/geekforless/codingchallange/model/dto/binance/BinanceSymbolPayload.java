package com.geekforless.codingchallange.model.dto.binance;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class BinanceSymbolPayload {
    List<SymbolPayload> symbols;
}
