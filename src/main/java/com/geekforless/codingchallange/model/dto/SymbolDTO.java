package com.geekforless.codingchallange.model.dto;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
public class SymbolDTO {
    private String symbol;
    private BigDecimal median;
    private BigDecimal recentSeenPrice;
    private Integer seenCount;

    @Override
    public String toString() {
        return "SymbolDTO{" +
                "symbol='" + symbol + '\'' +
                ", median=" + median +
                ", recentSeenPrice=" + recentSeenPrice +
                ", seenCount=" + seenCount +
                '}';
    }
}
