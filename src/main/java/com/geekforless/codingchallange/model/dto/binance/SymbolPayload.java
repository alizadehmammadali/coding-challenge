package com.geekforless.codingchallange.model.dto.binance;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SymbolPayload {
    private String symbol;
    private String status;
}
