package com.geekforless.codingchallange.model.dto.binance.statistic;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Comparator;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.concurrent.ConcurrentHashMap;

public class SymbolStatisticDTO {
    private String symbol;
    private Queue<BigDecimal> minHeap = new PriorityQueue<>();
    private Queue<BigDecimal> maxHeap = new PriorityQueue<>(Comparator.reverseOrder());
    private final Map<BigDecimal, Integer> recentOnes = new ConcurrentHashMap<>();

    public void add(BigDecimal num) {
        recentOnes.put(num, recentOnes.getOrDefault(num, 0) + 1);
        if (minHeap.size() == maxHeap.size()) {
            maxHeap.offer(num);
            minHeap.offer(maxHeap.poll());
        } else {
            minHeap.offer(num);
            maxHeap.offer(minHeap.poll());
        }
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getSymbol() {
        return symbol;
    }

    public BigDecimal getMedian() {
        BigDecimal median;
        if (minHeap.size() > maxHeap.size()) {
            median = minHeap.peek();
        } else {
            assert minHeap.peek() != null;
            median = (minHeap.peek().add(maxHeap.peek())).divide(BigDecimal.valueOf(2), RoundingMode.HALF_DOWN);
        }
        return median;
    }

    public Integer getSeenCount() {
        return maxHeap.size() + minHeap.size();
    }

    public BigDecimal getRecentSeenPrice() {
        int max = Integer.MIN_VALUE;
        BigDecimal recentPrice = null;
        for (Map.Entry<BigDecimal, Integer> entry : recentOnes.entrySet()) {
            if (entry.getValue() > max) {
                max = entry.getValue();
                recentPrice = entry.getKey();
            }
        }
        return recentPrice;
    }

}
