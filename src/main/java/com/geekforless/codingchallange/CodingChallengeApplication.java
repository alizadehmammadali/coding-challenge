package com.geekforless.codingchallange;

import com.geekforless.codingchallange.service.BinanceTradeService;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients(basePackages = "com.geekforless.codingchallange.client")
@RequiredArgsConstructor
public class CodingChallengeApplication implements CommandLineRunner {

    private final BinanceTradeService binanceTradeService;

    public static void main(String[] args) {
        SpringApplication.run(CodingChallengeApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        binanceTradeService.getTradesBySymbols();
    }
}
