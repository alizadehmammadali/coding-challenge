package com.geekforless.codingchallange.service.impl;

import com.binance4j.websocket.client.WebsocketTradeClient;
import com.geekforless.codingchallange.client.BinanceClient;
import com.geekforless.codingchallange.mapper.SymbolMapper;
import com.geekforless.codingchallange.model.dto.binance.SymbolPayload;
import com.geekforless.codingchallange.model.dto.SymbolDTO;
import com.geekforless.codingchallange.model.dto.binance.statistic.SymbolStatisticDTO;
import com.geekforless.codingchallange.service.BinanceTradeService;
import com.geekforless.codingchallange.service.StatisticCollectorService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class BinanceTradeServiceImpl implements BinanceTradeService {

    private final BinanceClient binanceClient;

    private final StatisticCollectorService statisticCollectorService;

    public void getTradesBySymbols() {
        List<SymbolPayload> symbols = binanceClient.getSymbols().getSymbols();

        List<String> part1Symbols = symbols.subList(0, 1100)
                .stream()
                .map(SymbolPayload::getSymbol)
                .collect(Collectors.toList());

        List<String> part2Symbols = symbols.subList(1100, symbols.size())
                .stream()
                .map(SymbolPayload::getSymbol)
                .collect(Collectors.toList());

        getTrades(part1Symbols);

        getTrades(part2Symbols);
    }

    private void getTrades(List<String> symbols) {
        WebsocketTradeClient client = new WebsocketTradeClient(symbols);
        client.onOpen(cb -> {
            System.out.println("Socket has been opened with message: " + cb.message() + " code: " + cb.code());
        });
        client.onClosing(cb -> {
            System.out.println("Socket has been closing with reason:" + cb.getReason() + " code: " + cb.getCode());
        });
        client.onClosed(cb -> {
            System.out.println("Socket has been closed with reason: " + cb.getReason() + " code: " + cb.getCode());
        });
        // Connection failed
        client.onFailure(cb -> {
            System.out.println("Socket has been failed to get data with message: " + cb.getMessage());
        });
        // Payload received
        client.onMessage(cb -> {
            statisticCollectorService.collectAllSymbolsStatistics(cb.getSymbol(), cb.getPrice());
        });
        // Open the stream
        client.open();
    }

    @Override
    public List<SymbolPayload> getSymbols() {
        return binanceClient.getSymbols().getSymbols();
    }

    @Override
    public SymbolDTO getSymbolStatistics(String symbol) {
        return SymbolMapper.INSTANCE.toTDO(statisticCollectorService.getAllSymbolsStatistics().get(symbol));
    }


}
