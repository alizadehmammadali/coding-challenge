package com.geekforless.codingchallange.service;

import com.geekforless.codingchallange.model.dto.binance.statistic.SymbolStatisticDTO;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

@Service
public class StatisticCollectorService {
    private final Map<String, SymbolStatisticDTO> allSymbolsStatistics = new HashMap<>();

    public void collectAllSymbolsStatistics(String symbol, BigDecimal price){
        SymbolStatisticDTO statisticSymbolDTO = allSymbolsStatistics.getOrDefault(symbol, new SymbolStatisticDTO());
        statisticSymbolDTO.setSymbol(symbol);
        statisticSymbolDTO.add(price);
        allSymbolsStatistics.put(symbol,statisticSymbolDTO);
    }

    public Map<String, SymbolStatisticDTO> getAllSymbolsStatistics() {
        return allSymbolsStatistics;
    }
}
