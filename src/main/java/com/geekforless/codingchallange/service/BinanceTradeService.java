package com.geekforless.codingchallange.service;


import com.geekforless.codingchallange.model.dto.binance.SymbolPayload;
import com.geekforless.codingchallange.model.dto.SymbolDTO;

import java.util.List;

public interface BinanceTradeService {

    void getTradesBySymbols();

    List<SymbolPayload> getSymbols();

    SymbolDTO getSymbolStatistics(String symbol);
}
