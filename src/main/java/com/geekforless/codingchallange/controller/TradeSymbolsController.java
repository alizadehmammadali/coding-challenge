package com.geekforless.codingchallange.controller;

import com.geekforless.codingchallange.model.dto.binance.SymbolPayload;
import com.geekforless.codingchallange.model.dto.SymbolDTO;
import com.geekforless.codingchallange.service.BinanceTradeService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/symbols")
@RequiredArgsConstructor
public class TradeSymbolsController {

    private final BinanceTradeService binanceTradeService;

    @GetMapping
    public ResponseEntity<List<SymbolPayload>> getSymbols() {
        return ResponseEntity.ok(binanceTradeService.getSymbols());
    }


    @GetMapping("/{symbol}")
    public ResponseEntity<SymbolDTO> getSymbolStatistics(@PathVariable("symbol") String symbol) {
        return ResponseEntity.ok(binanceTradeService.getSymbolStatistics(symbol));
    }


}
