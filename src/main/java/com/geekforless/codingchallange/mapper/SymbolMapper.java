package com.geekforless.codingchallange.mapper;

import com.geekforless.codingchallange.model.dto.SymbolDTO;
import com.geekforless.codingchallange.model.dto.binance.statistic.SymbolStatisticDTO;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

@Mapper(unmappedSourcePolicy = ReportingPolicy.IGNORE)
public abstract class SymbolMapper {

    public static final SymbolMapper INSTANCE = Mappers.getMapper(SymbolMapper.class);

    public abstract SymbolDTO toTDO(SymbolStatisticDTO symbolStatisticDTO);

}
